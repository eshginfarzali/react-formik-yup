import * as yup from "yup";

export const validationSchema = yup.object().shape({
  firstName: yup
    .string()
    .required("This field is required")
    .matches(/^[A-Za-z ]+$/i, "Only alphabet characters and spaces are allowed"),
  lastName: yup
    .string()
    .required("This field is required")
    .matches(/^[A-Za-z ]+$/i, "Only alphabet characters and spaces are allowed"),
  age: yup
    .number()
    .typeError("Age must be a number")
    .integer("Age must be an integer")
    .min(16, "Age must be at least 16")
    .max(99, "Age must be at most 99"),
  notes: yup
    .string()
    .required("This field is required")
    .max(254, "Notes cannot exceed 254 characters"),
});
