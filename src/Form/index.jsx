import  { useState } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import {validationSchema} from '../validate'
import "./form.css";

export const Form = () => {


  const {
    register,
    handleSubmit,
    reset,
    formState: { isValid, isDirty,errors},
    
  } = useForm({
    mode: "onChange",
    resolver: yupResolver(validationSchema),
  });

  const [formData, setFormData] = useState({
    firstName: "",
    lastName: "",
    age: "",
    employed: false,
    favoriteColor: "",
    sauces: {
      ketchup: false,
      mustard: false,
      mayonnaise: false,
      guacamole: false,
    },
    stooge: "",
    notes: "",
  });

  const updateFormData = (name, value) => {
    if (name.startsWith("sauces.")) {
      const sauceName = name.replace("sauces.", "");
      setFormData((prevData) => ({
        ...prevData,
        sauces: {
          ...prevData.sauces,
          [sauceName]: value,
        },
      }));
    } else {
      setFormData((prevData) => ({
        ...prevData,
        [name]: value,
      }));
    }
  };

  const onSubmit = (data) => {
    console.log(data); 
  };

  return (
    <div className="form-container">
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="form-group">
          <label htmlFor="firstName">First Name</label>
          <input
            id="firstName"
            className="form-input"
            placeholder="First Name"
            {...register("firstName")}
            onChange={(e) => {
              updateFormData("firstName", e.target.value);
            }}
          />
          {errors.firstName && (
            <p className="error-message">{errors.firstName.message}</p>
          )}
        </div>

        <div className="form-group">
          <label htmlFor="lastName">Last Name</label>
          <input
            id="lastName"
            className="form-input"
            placeholder="Last Name"
            {...register("lastName")}
            onChange={(e) => {
              updateFormData("lastName", e.target.value);
            }}
          />
          {errors.lastName && (
            <p className="error-message">{errors.lastName.message}</p>
          )}
        </div>

        <div className="form-group">
          <label htmlFor="age">Age</label>
          <input
            id="age"
            className="form-input"
            placeholder="Age"
            type="number"
            {...register("age")}
            onChange={(e) => {
              updateFormData("age", e.target.value);
            }}
          />
          {errors.age && (
            <p className="error-message">{errors.age.message}</p>
          )}
        </div>

        <div className="form-group">
          <label>Employed</label>
          <input
            id="employed"
            type="checkbox"
            {...register("employed")}
            onChange={(e) => updateFormData("employed", e.target.checked)}
          />
        </div>

        <div className="form-group">
          <label>Favorite Color</label>
          <input
            id="favoriteColor"
            type="color"
            {...register("favoriteColor")}
            onChange={(e) => updateFormData("favoriteColor", e.target.value)}
          />
        </div>

        <div className="form-group">
          <label>Sauces</label>
          <div className="checkbox-group">
            <input
              id="ketchup"
              type="checkbox"
              {...register("sauces.ketchup")}
              onChange={(e) => updateFormData("sauces.ketchup", e.target.checked)}
            />
            <label htmlFor="ketchup">Ketchup</label>
            <br />

            <input
              id="mustard"
              type="checkbox"
              {...register("sauces.mustard")}
              onChange={(e) => updateFormData("sauces.mustard", e.target.checked)}
            />
            <label htmlFor="mustard">Mustard</label>
            <br />

            <input
              id="mayonnaise"
              type="checkbox"
              {...register("sauces.mayonnaise")}
              onChange={(e) => updateFormData("sauces.mayonnaise", e.target.checked)}
            />
            <label htmlFor="mayonnaise">Mayonnaise</label>
            <br />

            <input
              id="guacamole"
              type="checkbox"
              {...register("sauces.guacamole")}
              onChange={(e) => updateFormData("sauces.guacamole", e.target.checked)}
            />
            <label htmlFor="guacamole">Guacamole</label>
          </div>
        </div>

        <div className="form-group">
          <label>Best Stooge</label>
          <div className="radio-group">
            <label>
              <input
                {...register("stooge")}
                type="radio"
                value="Larry"
                onChange={() => updateFormData("stooge", "Larry")}
              />
              Larry
            </label>
            <br />
            <label>
              <input
                {...register("stooge")}
                type="radio"
                value="Moe"
                onChange={() => updateFormData("stooge", "Moe")}
              />
              Moe
            </label>
            <br />
            <label>
              <input
                {...register("stooge")}
                type="radio"
                value="Curly"
                onChange={() => updateFormData("stooge", "Curly")}
              />
              Curly
            </label>
          </div>
        </div>

        <div className="form-group">
          <label>Notes</label>
          <textarea
            className="form-textarea"
            placeholder="Note"
            {...register("notes")}
            onChange={(e) => {
              updateFormData("notes", e.target.value);
            }}
          />
          {errors?.notes && (
            <p className="error-message">{errors.notes.message}</p>
          )}
        </div>

        <div className="btn-group">
          <input type="submit" disabled={!isValid} className="btn-submit" />
          <input
            type="reset"
            onClick={() => {
              reset();
              setFormData({
                firstName: "",
                lastName: "",
                age: "",
                employed: false,
                favoriteColor: "",
                sauces: {
                  ketchup: false,
                  mustard: false,
                  mayonnaise: false,
                  guacamole: false,
                },
                stooge: "",
                notes: "",
              });
            }}
            disabled={!isDirty}
            className="btn-reset"
          />
        </div>
      </form>

      <div className="data">
        <h2>Form data</h2>
        <pre>{JSON.stringify(formData, null, 2)}</pre>
      </div>
    </div>
  );
};
